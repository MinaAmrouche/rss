package com.epitech.java.utility;

import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class Utils  {

   public void loadNewScene(String scenePath, ActionEvent event){
        try{
            Parent accountView = FXMLLoader.load(getClass().getResource(scenePath));
            Scene scene = new Scene(accountView);

            Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            stage.setScene(scene);
            stage.show();
        }
        catch (IOException ex){
            System.out.println(ex.getMessage());
        }
    }
}
