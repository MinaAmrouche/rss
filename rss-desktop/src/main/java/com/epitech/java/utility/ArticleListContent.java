/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epitech.java.utility;

/**
 *
 * @author Erik
 */
public class ArticleListContent {
    private String title;
    private String preview;
    private String image;
    private String content;

    public ArticleListContent(String title, String preview, String image, String content) {
        this.title = title;
        this.preview = preview;
        this.image = image;
        this.content = content;
    }

    public ArticleListContent(String title, String preview, String image) {
        this.title = title;
        this.preview = preview;
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPreview() {
        return preview;
    }

    public void setPreview(String preview) {
        this.preview = preview;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}