/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epitech.java.utility;

/**
 *
 * @author Erik
 */
public class RssFeed {
    private String feedName;
    private String feedUrl;

    public RssFeed() {
    }

    public RssFeed(String feedName, String feedUrl) {
        this.feedName = feedName;
        this.feedUrl = feedUrl;
    }

    public RssFeed(String url) {
        this.feedName = url;
    }

    public String getFeedName() {
        return this.feedName;
    }

    public String getFeedUrl() {
        return this.feedUrl;
    }

    public void setFeedName(String feedName) {
        this.feedName = feedName;
    }

    public void setFeedUrl(String feedUrl) {
        this.feedUrl = feedUrl;
    }
}