/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epitech.java;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import com.epitech.java.model.User;
import com.epitech.java.services.BDDRequest;
import com.epitech.java.utility.Utils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import javax.swing.*;


/**
 *
 * @author Erik
 */
public class CreateAccountController implements Initializable {
    
    @FXML
    private TextField username;
    
    @FXML
    private TextField email;
    
    @FXML 
    private PasswordField password;
    
    @FXML
    private  void back(ActionEvent event){
        Utils utils = new Utils();
        utils.loadNewScene("/fxml/Login.fxml" , event);
    }

    @FXML
    private void handleButtonAction(ActionEvent event) {
        System.out.println(username.getText());
        System.out.println(email.getText());
        System.out.println(password.getText());

        //Handle Account Creation

        User user = new User(username.getText(), email.getText(), password.getText());

        if (BDDRequest.sendPostRequest(user, BDDRequest.SIGN_UP_URL) == true) {

            // Response response = invocationBuilder.post(Entity.json());

            Utils utils = new Utils();
            utils.loadNewScene("/fxml/Login.fxml" , event);
            } else {
            JOptionPane.showMessageDialog(null, "Sign up was unsuccessful!");
        }
    }
    
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }
}
