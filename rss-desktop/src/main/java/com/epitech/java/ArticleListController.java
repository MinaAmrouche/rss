/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epitech.java;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import com.epitech.java.utility.ArticleListContent;
import com.epitech.java.utility.Utils;
import com.sun.syndication.feed.synd.SyndEntry;
import com.sun.syndication.feed.synd.SyndFeed;
import com.sun.syndication.io.FeedException;
import com.sun.syndication.io.SyndFeedInput;
import com.sun.syndication.io.XmlReader;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.collections.ObservableList;
import javafx.collections.FXCollections;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ListCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.util.Callback;

/**
 * FXML Controller class
 *
 * @author Erik
 */
public class ArticleListController implements Initializable {

    @FXML
    private ListView<ArticleListContent> articleList;
    private final ObservableList<ArticleListContent> data = FXCollections.observableArrayList();


    @FXML
    private Label feedName;
    
    @FXML
    private void disconnect(ActionEvent event) {
        Utils utils = new Utils();
        utils.loadNewScene("/fxml/Login.fxml" , event);
    }

    public void setFeedName(String feedName){
        this.feedName.setText(feedName);
    }

    @FXML
    private void back(ActionEvent event) {
        Utils utils = new Utils();
        utils.loadNewScene("/fxml/FeedList.fxml" , event);
    }

    @FXML
    private void handleListClick(MouseEvent event) {
        System.out.println(articleList.getSelectionModel().getSelectedItem().getTitle());
        System.out.println(articleList.getSelectionModel().getSelectedItem().getContent());
        System.out.println(articleList.getSelectionModel().getSelectedItem().getImage());

        try{
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/Article.fxml"));
            Parent articleView = (Parent) loader.load();
            ArticleController articleController = (ArticleController) loader.getController();
            if (articleController != null) {
                articleController.setArticleTitle(articleList.getSelectionModel().getSelectedItem().getTitle());
                articleController.setArticleContent(articleList.getSelectionModel().getSelectedItem().getContent());
                articleController.setArticleImage(articleList.getSelectionModel().getSelectedItem().getImage());
            } else {
                System.out.println("Oups !");
            }
            System.out.println("Ohla");
            Scene scene = new Scene(articleView);
            Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            stage.setScene(scene);
            stage.show();
        }
        catch (IOException ex){
            System.out.println(ex.getMessage());
        }
    }

    /**
     * Initializes the controller class.
     */
    public void initialize(URL url, ResourceBundle rb) {

        data.clear();
        // Ajouter les article a data pour les display, il va peut être falloir adapter la classe ArticleListContent

        String feedUrl = this.feedName.getText();
        XmlReader reader = null;

        try {
            URL furl = new URL(feedUrl);
            reader = new XmlReader(furl);
            SyndFeed feed = new SyndFeedInput().build(reader);
            List<SyndEntry> articlesList = feed.getEntries();
            if (!articlesList.isEmpty()) {
                for (SyndEntry article : articlesList) {
                    data.add(new ArticleListContent(article.getTitle(), article.getDescription().getValue(), "https://athlonecommunityradio.ie/wp-content/uploads/2017/04/placeholder.png", article.getLink()));
                }
            }
        } catch (MalformedURLException ex) {
            Logger.getLogger(ArticleListController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ArticleListController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalArgumentException ex) {
            Logger.getLogger(ArticleListController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (FeedException ex) {
            Logger.getLogger(ArticleListController.class.getName()).log(Level.SEVERE, null, ex);
        }


        articleList.setCellFactory(new Callback<ListView<ArticleListContent>, ListCell<ArticleListContent>>(){

            @Override
            public ListCell<ArticleListContent> call(ListView<ArticleListContent> param) {
                ListCell<ArticleListContent> cell = new ListCell<ArticleListContent>() {
                    @Override
                    protected void updateItem(ArticleListContent article, boolean btl) {
                        super.updateItem(article, btl);
                        if (article != null) {
                            Image image = new Image(article.getImage());
                            ImageView imageView = new ImageView(image);
                            setGraphic(imageView);
                            setText(article.getPreview());
                        }
                    }
                };
                return cell;
            }

        });
        articleList.setItems(data);
    }
}
