/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epitech.java;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import com.epitech.java.model.User;
import com.epitech.java.services.BDDRequest;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.PasswordField;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import javax.swing.*;

/**
 *
 * @author Erik
 */
public class LoginController implements Initializable {

    private User user;

    @FXML
    private TextField email;
    
    @FXML 
    private PasswordField password;

    @FXML
    private void connect(ActionEvent event) {
        System.out.println(email.getText());
        System.out.println(password.getText());

        //Utilise les variable email et password pour obtenir la valeur des input de texte
        user = BDDRequest.signIn(email.getText(), password.getText());

        if (user == null){
            Label message = new Label("Sign in was unsuccessful!");
            JOptionPane.showMessageDialog(null, "Sign in was unsuccessful!");
        } else {
            try {
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml//FeedList.fxml"));
                Parent accountView = loader.load();
                FeedListController feedListController = loader.getController();
                if (feedListController != null){
                    feedListController.setUser(user);
                }
                Scene scene = new Scene(accountView);
                Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                stage.setScene(scene);
                stage.show();
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }
    
    @FXML
    private void handleAccountCreation(ActionEvent event) {
        //Method pour aller à la page de création de compte, la gestion n'est pas faite ici
        try{
            Parent accountView = FXMLLoader.load(getClass().getResource("/fxml/CreateAccount.fxml"));
            Scene scene = new Scene(accountView);
            Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();        
            stage.setScene(scene);
            stage.show();
        }
        catch (IOException e){
            System.out.println(e.getMessage());
        }
    }
    
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
}
