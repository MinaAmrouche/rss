/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epitech.java;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * FXML Controller class
 *
 * @author Erik
 */
public class ArticleController implements Initializable {

    @FXML
    private Label title;
    
    @FXML
    private Label content;
    
    @FXML
    private ImageView image;
    
    
    @FXML
    private void back(ActionEvent event) {
        try{
            Parent accountView = FXMLLoader.load(getClass().getResource("/fxml/ArticleList.fxml"));
            Scene scene = new Scene(accountView);

            Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();        
            stage.setScene(scene);
            stage.show();
        }
        catch (IOException ex){
            System.out.println(ex.getMessage());
        }
    }
    
    @FXML
    private void disconnect(ActionEvent event) {
        try{
            Parent accountView = FXMLLoader.load(getClass().getResource("/fxml/Login.fxml"));
            Scene scene = new Scene(accountView);

            Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();        
            stage.setScene(scene);
            stage.show();
        }
        catch (IOException ex){
            System.out.println(ex.getMessage());
        }
    }
    
    public void setArticleTitle(String title) {
        this.title.setText(title);
    }
    
    public void setArticleContent(String content) {
        this.content.setText(content);
    }
    
    public void setArticleImage(String url) {
        Image image = new Image(url);
        this.image.setImage(image);
    }
    /**
     * Initializes the controller class.
     */
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
}
