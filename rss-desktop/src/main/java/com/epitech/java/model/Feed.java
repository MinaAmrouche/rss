package com.epitech.java.model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Feed {
    private int id;
    private String feed;

    public Feed() {
    }

    public Feed(int id, String feed) {
        this.id = id;
        this.feed = feed;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFeed() {
        return feed;
    }

    public void setFeed(String feed) {
        this.feed = feed;
    }
}
