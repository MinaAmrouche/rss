package com.epitech.java.services;

import com.epitech.java.model.Feed;
import com.epitech.java.model.User;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;
import com.sun.jersey.api.client.WebResource;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class BDDRequest {

    public static final String BASE_URL = "http://212.227.53.116:8080";
    public static final String SIGN_UP_URL =  BASE_URL + "/user/signUp";
    public static final String SIGN_IN_URL = BASE_URL + "/user/signIn";
    public static final String SIGN_OUT_URL = BASE_URL + "/user/logout";
    public static final String ADD_FEED_URL = BASE_URL + "/feed/add";
    public static final String GET_ALL_FEED_URL = BASE_URL + "/feed/getAll"; //2001
    public static final String DELETE_FEED_URL = BASE_URL + "/feed/del";

    public static boolean sendPostRequest(Object object, String apiPath){
        String param = new JSONObject(object).toString();
        System.out.println("Post param : " + param);
        Client client = Client.create();
        WebResource webResource = client.resource(apiPath);
        ClientResponse response = webResource.type("application/json").post(ClientResponse.class, param);

        if (response.getStatus() == 201){
            return true;
        } else {
            return false;
        }
    }

    public static User signIn(String email, String password){
        String path = SIGN_IN_URL + "?email=" + email + "&password=" + password;
        Client client = Client.create();
        WebResource webResource = client.resource(path);
        ClientResponse response = webResource.type("application/json").post(ClientResponse.class);
        try {
            String entity = response.getEntity(String.class);
            if (response.getStatus() == 200) {
                JSONObject json = new JSONObject(entity);
                return new User(
                        json.getInt("id"),
                        json.getString("userName"),
                        json.getString("email"),
                        json.getString("token"));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static List<Feed> getAllFeed(String token){
        Client client = Client.create();
        WebResource webResource = client.resource(GET_ALL_FEED_URL);
        ClientResponse response = webResource.header("token", token).type("application/json").get(ClientResponse.class);

        if (response.getStatus() == 200){
            List<Feed> entity = response.getEntity( new GenericType<List<Feed>>(){});
            return (entity);
        }
        return new ArrayList<Feed>();
    }

    public static boolean addFeed(String token, Feed feed){
        Client client = Client.create();
        WebResource webResource = client.resource(ADD_FEED_URL);
        ClientResponse response = webResource.header(token, token).type("application/json").post(ClientResponse.class, feed);
        if (response.getStatus() == 2001){
            return true;
        }
        return false;
    }

    public static boolean deleteFeed(String token, int feedId){

        return true;
    }

    public static Feed getFeedById(){

        return new Feed();
    }

    public static boolean signOut(String token){
        Client client = Client.create();
        WebResource webResource = client.resource(SIGN_OUT_URL);
        ClientResponse response = webResource.header("token", token).type("applicationn/json").get(ClientResponse.class);
        if (response.getStatus() == 200) {
            return true;
        }
        return false;
    }

}
