/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epitech.java;

import java.net.URL;
import java.util.ResourceBundle;
import java.io.IOException;

import com.epitech.java.model.User;
import com.epitech.java.services.BDDRequest;
import com.epitech.java.utility.RssFeed;
import com.epitech.java.utility.Utils;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.util.Callback;


/**
 * FXML Controller class
 *
 * @author Erik
 */
public class FeedListController implements Initializable {


    private User user;

    @FXML
    private ListView<RssFeed> feedList;
    private final ObservableList<RssFeed> data = FXCollections.observableArrayList();
    
    @FXML
    private TextField textField;

    @FXML
    private void disconnect(ActionEvent event) {
        if (BDDRequest.signOut(user.getToken()) == true) {
            Utils utils = new Utils();
            utils.loadNewScene("/fxml/Login.fxml", event);
        }
    }
    
    @FXML
    private void addFeed(ActionEvent event) {
        data.add(new RssFeed(textField.getText()));
        textField.clear();
    }

    @FXML
    private void back(ActionEvent event) {
        try{
            Parent accountView = FXMLLoader.load(getClass().getResource("/fxml/Login.fxml"));
            Scene scene = new Scene(accountView);

            Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            stage.setScene(scene);
            stage.show();
        }
        catch (IOException ex){
            System.out.println(ex.getMessage());
        }
    }

    @FXML
    private void handleListClick(MouseEvent event) {
        System.out.println(feedList.getSelectionModel().getSelectedItem().getFeedName());

        try{
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/ArticleList.fxml"));
            Parent accountView = (Parent) loader.load();
            ArticleListController articleListController = (ArticleListController) loader.getController();
            if (articleListController != null) {
                articleListController.setFeedName(feedList.getSelectionModel().getSelectedItem().getFeedName());
            } else {
                System.out.println("Oups !");
            }

            System.out.println("Ohla");
            Scene scene = new Scene(accountView);
            Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            stage.setScene(scene);
            stage.show();
        }
        catch (IOException ex){
            System.out.println(ex.getMessage());
        }
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        BDDRequest.getAllFeed(user.getToken());
        data.clear();
        // Pour l'instant je set le nom du feed avec l'url, je te laisse changer la classe RssFeed pour que ça colle avec un feed :)
        data.add(new RssFeed("https://s-media-cache-ak0.pinimg.com/originals/b0/91/6c/b0916c20e9e00c65d093aa22a6575abf.jpg"));
        data.add(new RssFeed("http://air-agence.com/wp-content/uploads/2016/05/montagne.png"));
        data.add(new RssFeed("http://www.dpesca.com/media/cache/256/256/n/w/nwm-kp256x256-233229226img-3301-P%C3%AAche-au-Gros-%C3%A0-Rovinj,-Mer-Adriatique.jpg"));
        data.add(new RssFeed("https://www.autoecole-haas.fr/file/2017/04/Auto-Ecole-Haas-enseignement-e1491381807649.png"));
        data.add(new RssFeed("http://mostfamousperson.net/NickiMinaj.png"));

        feedList.setCellFactory(new Callback<ListView<RssFeed>, ListCell<RssFeed>>(){

            @Override
            public ListCell<RssFeed> call(ListView<RssFeed> param) {
                ListCell<RssFeed> cell = new ListCell<RssFeed>() {
                    @Override
                    protected void updateItem(final RssFeed feed, boolean btl) {
                        super.updateItem(feed, btl);
                        if (feed != null) {
                            setText(feed.getFeedName());
                            Button btn = new Button("(x)");
                            btn.setOnAction(new EventHandler<ActionEvent>() {
                                @Override
                                public void handle(ActionEvent event) {
                                    data.remove(feed);
                                }
                            });
                            setGraphic(btn);
                        }
                        if (feed == null) {
                            setGraphic(null);
                            setText(null);
                        }
                    }
                };
                return cell;
            }
        });
        feedList.setItems(data);
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }


}
