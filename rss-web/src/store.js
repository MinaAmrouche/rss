import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import axios from 'axios';
import { multiClientMiddleware } from 'redux-axios-middleware';

import rootReducer from './reducers';

const clients = {
    default: {
        client: axios.create({
            baseURL: 'http://212.227.53.116:8080/',
            responseType: 'json',
        })
    },
    rss2json: {
        client: axios.create({
            baseURL: 'https://rss2json.com/',
            responseType: 'json'
        })
    }
}

let store = createStore(
    rootReducer,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
    applyMiddleware(
        thunk,
        multiClientMiddleware(clients)
    )
);

export default store;