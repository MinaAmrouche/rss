import { combineReducers } from 'redux';
import auth from './auth';
import feeds from './feeds';

const rootReducer = combineReducers({
    auth,
    feeds,
});

export default rootReducer;
