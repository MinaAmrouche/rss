import {
    GET_FEEDS_PENDING,
    GET_FEEDS_SUCCESS,
    GET_FEEDS_ERROR,
    DELETE_FEED_PENDING,
    DELETE_FEED_SUCCESS,
    DELETE_FEED_ERROR,
    ADD_FEED_PENDING,
    ADD_FEED_SUCCESS,
    ADD_FEED_ERROR,
    GET_FEED_PENDING,
    GET_FEED_SUCCESS,
    GET_FEED_ERROR,
} from '../actions';

const initialState = {
    isGetFeedsSuccess: false,
    isGetFeedsPending: false,
    isGetFeedsError: false,
    isDeleteFeedSuccess: false,
    isDeleteFeedPending: false,
    isDeleteFeedError: false,
    isAddFeedSuccess: false,
    isAddFeedPending: false,
    isAddFeedError: false,
    isGetFeedSuccess: false,
    isGetFeedPending: false,
    isGetFeedError: false,
    feeds: [],
    feedContent: null,
};

export default function auth(state = initialState, action) {
    switch (action.type) {
        case GET_FEEDS_PENDING:
            return {
                ...state,
                isGetFeedsPending: true,
            };
        case GET_FEEDS_SUCCESS:
            return {
                ...state,
                isGetFeedsSuccess: true,
                isGetFeedsPending: false,
                feeds: action.payload.data
            };
        case GET_FEEDS_ERROR:
            return {
                ...state,
                isGetFeedsError: true,
                isGetFeedsPending: false
            };
        case DELETE_FEED_PENDING:
            return {
                ...state,
                isDeleteFeedPending: true,
            };
        case DELETE_FEED_SUCCESS:
            return {
                ...state,
                isDeleteFeedSuccess: true,
                isDeleteFeedPending: false,
            };
        case DELETE_FEED_ERROR:
            return {
                ...state,
                isDeleteFeedError: true,
                isDeleteFeedPending: false
            };
        case ADD_FEED_PENDING:
            return {
                ...state,
                isAddFeedPending: true,
            };
        case ADD_FEED_SUCCESS:
            return {
                ...state,
                isAddFeedSuccess: true,
                isAddFeedPending: false,
            };
        case ADD_FEED_ERROR:
            return {
                ...state,
                isAddFeedError: true,
                isAddFeedPending: false
            };
        case GET_FEED_PENDING:
            return {
                ...state,
                isGetFeedPending: true,
            };
        case GET_FEED_SUCCESS:
            return {
                ...state,
                isGetFeedSuccess: true,
                isGetFeedPending: false,
                feedContent: action.payload.data.items,
            };
        case GET_FEED_ERROR:
            return {
                ...state,
                isGetFeedError: true,
                isGetFeedPending: false
            };
        default:
            return state;
    }
}
