import {
    SIGNIN_PENDING,
    SIGNIN_SUCCESS,
    SIGNIN_ERROR,

    SIGNUP_PENDING,
    SIGNUP_SUCCESS,
    SIGNUP_ERROR,

    SIGNOUT_PENDING,
    SIGNOUT_SUCCESS,
    SIGNOUT_ERROR,
} from '../actions';

const initialState = {
    isSigninSuccess: false,
    isSigninPending: false,
    isSigninError: false,
    isSignupSuccess: false,
    isSignupPending: false,
    isSignupError: false,
    isSignoutSuccess: false,
    isSignoutPending: false,
    isSignoutError: false,
    token: null,
};

export default function auth(state = initialState, action) {
    switch (action.type) {
        case SIGNIN_PENDING:
            return {
                ...state,
                isSigninPending: true,
            };
        case SIGNIN_SUCCESS:
            const { token } = action.payload.data;
            return {
                ...state,
                isSigninSuccess: true,
                isSigninPending: false,
                token,
            };
        case SIGNIN_ERROR:
            return {
                ...state,
                isSigninError: true,
                isSigninPending: false
            };
        case SIGNUP_PENDING:
            return {
                ...state,
                isSignupPending: true,
            };
        case SIGNUP_SUCCESS:
            return {
                ...state,
                isSignupSuccess: true,
                isSignupPending: false,
            };
        case SIGNUP_ERROR:
            return {
                ...state,
                isSignupError: true,
                isSignupPending: false,
            };
        case SIGNOUT_PENDING:
            return {
                ...state,
                isSignoutPending: true,
            };
        case SIGNOUT_SUCCESS:
            return {
                ...state,
                isSignoutSuccess: true,
                isSignoutPending: false,
                token: '',
            };
        case SIGNOUT_ERROR:
            return {
                ...state,
                isSignoutError: true,
                isSignoutPending: false,
            };
        default:
            return state;
    }
}
