export const SIGNIN_PENDING = 'SIGNIN_PENDING';
export const SIGNIN_SUCCESS = 'SIGNIN_SUCCESS';
export const SIGNIN_ERROR = 'SIGNIN_ERROR';

export const SIGNUP_PENDING = 'SIGNUP_PENDING';
export const SIGNUP_SUCCESS = 'SIGNUP_SUCCESS';
export const SIGNUP_ERROR = 'SIGNUP_ERROR';

export const SIGNOUT_PENDING = 'SIGNOUT_PENDING';
export const SIGNOUT_SUCCESS = 'SIGNOUT_SUCCESS';
export const SIGNOUT_ERROR = 'SIGNOUT_ERROR';

export function signIn(email, password) {
    return {
        types: [SIGNIN_PENDING, SIGNIN_SUCCESS, SIGNIN_ERROR],
        payload: {
            client: 'default',
            request: {
                method: 'POST',
                url: '/user/signIn?email=' + email + '&password=' + password,
            },
        },
    };
}

export function signUp(username, email, password) {
    return {
        types: [SIGNUP_PENDING, SIGNUP_SUCCESS, SIGNUP_ERROR],
        payload: {
            client: 'default',
            request: {
                method: 'POST',
                url: '/user/signUp',
                data: {
                    userName: username,
                    email,
                    password,
                }
            },
        },
    };
}

export function signOut(token) {
    return {
        types: [SIGNOUT_PENDING, SIGNOUT_SUCCESS, SIGNOUT_ERROR],
        payload: {
            client: 'default',
            request: {
                method: 'GET',
                url: '/user/logout',
                headers: {
                    token
                }
            },
        },
    };
}
