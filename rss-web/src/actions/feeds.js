export const GET_FEEDS_PENDING = 'GET_FEEDS_PENDING';
export const GET_FEEDS_SUCCESS = 'GET_FEEDS_SUCCESS';
export const GET_FEEDS_ERROR = 'GET_FEEDS_ERROR';

export const DELETE_FEED_PENDING = 'DELETE_FEED_PENDING';
export const DELETE_FEED_SUCCESS = 'DELETE_FEED_SUCCESS';
export const DELETE_FEED_ERROR = 'DELETE_FEED_ERROR';

export const ADD_FEED_PENDING = 'ADD_FEED_PENDING';
export const ADD_FEED_SUCCESS = 'ADD_FEED_SUCCESS';
export const ADD_FEED_ERROR = 'ADD_FEED_ERROR';

export const GET_FEED_PENDING = 'GET_FEED_PENDING';
export const GET_FEED_SUCCESS = 'GET_FEED_SUCCESS';
export const GET_FEED_ERROR = 'GET_FEED_ERROR';

export function getAllFeeds(token) {
    return {
        types: [GET_FEEDS_PENDING, GET_FEEDS_SUCCESS, GET_FEEDS_ERROR],
        payload: {
            client: 'default',
            request: {
                method: 'GET',
                url: '/feed/getAll',
                headers: {
                    'token': token
                }
            },
        },
    };
}

export function deleteFeed(token, id) {
    return {
        types: [DELETE_FEED_PENDING, DELETE_FEED_SUCCESS, DELETE_FEED_ERROR],
        payload: {
            client: 'default',
            request: {
                method: 'DELETE',
                url: '/feed/delete?id=' + id,
                headers: {
                    'token': token
                }
            },
        },
    };
}

export function addFeed(token, url) {
    return {
        types: [ADD_FEED_PENDING, ADD_FEED_SUCCESS, ADD_FEED_ERROR],
        payload: {
            client: 'default',
            request: {
                method: 'POST',
                url: 'feed/add?feed=' + url,
                headers: {
                    'token': token
                }
            },
        },
    };
}

export function getFeedContent(url) {
    return {
        types: [GET_FEED_PENDING, GET_FEED_SUCCESS, GET_FEED_ERROR],
        payload: {
            client: 'rss2json',
            request: {
                method: 'GET',
                url: 'api.json?rss_url=' + encodeURIComponent(url),
            },
        },
    };
}