import React, {Component} from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';

import { signUp } from '../actions';
import { SIGN_IN } from '../routes';

class SignUp extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            email: '',
            password: ''
        };
    }

    handleSubmit = (e) => {
        e.preventDefault();
        let { username, email, password } = this.state;
        this.props.signUp(username, email, password);
    };

    handleChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        });
    };

    render() {
        let { username, email, password } = this.state;
        let { isSignupPending, isSignupSuccess, isSignupError } = this.props;

        if (isSignupSuccess) {
            return (
                <Redirect to={{ pathname: SIGN_IN }} />
            )
        }

        return (
            <div className="container">
                <form action="my_feeds.html" onSubmit={this.handleSubmit}>
                    <input type="username" placeholder="Username" name="username" required onChange={this.handleChange} value={username} />
                    <input type="email" placeholder="E-mail" name="email" required onChange={this.handleChange} value={email} />
                    <input type="password" placeholder="Password" name="password" required onChange={this.handleChange} value={password} />
                    <a href="my_feeds.html">
                        <button type="submit">Register</button>
                    </a>
                </form>
                {isSignupPending && <div>Please wait...</div>}
                {isSignupSuccess && <div>Success.</div>}
                {isSignupError && <div>Error...</div>}
            </div>
        )
    }
}

const mapStateToProps = ({ auth }) => {
    return {
        isSignupPending: auth.isSignupPending,
        isSignupSuccess: auth.isSignupSuccess,
        isSignupError: auth.isSignupError
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        signUp: (username, email, password) => dispatch(signUp(username, email, password))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(SignUp);
