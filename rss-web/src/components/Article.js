import React, { Component } from 'react';
import { connect } from 'react-redux';
import renderHTML from 'react-render-html';

import { getFeedContent } from '../actions';


class Article extends Component {
    constructor(props) {
        super(props);
        this.state = {
            url: decodeURIComponent(props.match.params.url),
            id: props.match.params.id,
        }
    }

    componentDidMount() {
        if (!this.props.feedContent) {
            this.props.getFeedContent(this.state.url);
        }
    }

    render() {
        let { id } = this.state;
        let { feedContent } = this.props;
        let article = {title: '', thumbnail: '', content: ''};
        if (feedContent) {
            article = this.props.feedContent[id];
        }

        return (
            <div>
                <h1>{article.title}</h1>
                <div className="imgcenter">
                    <img src={article.thumbnail} alt="Avatar" className="image_feed" />
                </div>
                <div className="article">
                    <p>{renderHTML(article.content)}</p>
                </div>
            </div>
        )
    }
}


const mapStateToProps = ({ feeds }) => {
    return {
        feedContent: feeds.feedContent,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        getFeedContent: (url) => dispatch(getFeedContent(url)),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Article);
