import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

import { MY_FEEDS } from '../routes';
import { addFeed } from '../actions';


class FeedCreation extends Component {
    constructor(props) {
        super(props);
        this.state = {
            url: '',
            success: false,
        };
    }

    handleSubmit = (e) => {
        e.preventDefault();
        let { url } = this.state;
        this.props.addFeed(this.props.token, url)
            .then(() => {
                this.setState({
                    success: true
                })
            });
    };

    handleChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        });
    };

    render() {
        let { url } = this.state;

        if (this.state.success) {
            return (
                <Redirect to={{ pathname: MY_FEEDS }} />
            )
        }

        return (
            <div className="container">
                <form action="my_feeds.html" onSubmit={this.handleSubmit}>
                    <input type="url" placeholder="Add RSS Link" name="url" required onChange={this.handleChange} value={url}/>
                    <button type="submit">Add</button>
                </form>
            </div>
        )
    }
}

const mapStateToProps = ({ auth, feeds }) => {
    return {
        token: auth.token,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        addFeed: (token, url) => dispatch(addFeed(token, url)),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(FeedCreation);
