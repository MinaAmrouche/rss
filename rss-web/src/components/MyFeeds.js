import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

import {
    FEED_CREATION,
    FEED,
    SIGN_IN,
} from '../routes';
import { signOut, getAllFeeds, deleteFeed } from '../actions';


class MyFeeds extends Component {

    constructor(props) {
        super(props);

        this.state = {
            signOutSuccess: false,
        }
    }

    componentDidMount() {
        this.props.getAllFeeds(this.props.token);
    }

    handleLogoutClick = (e) => {
        this.props.signOut(this.props.token)
            .then(() => {
                this.setState({ signOutSuccess: true })
            });
    };

    handleDeleteClick = (e) => {
        this.props.deleteFeed(this.props.token, e.target.name)
            .then(() => {
                this.props.getAllFeeds(this.props.token);
            });
    };

    render() {
        let { feeds } = this.props;
        let { signOutSuccess } = this.state;

        if (signOutSuccess) {
            return (
                <Redirect to={{ pathname: SIGN_IN }} />
            )
        }

        let rows;
        if (feeds) {
            rows = feeds.map((feed, index) => {
                return (
                    <tr key={index}>
                        <td>
                            <Link to={FEED + '/' + encodeURIComponent(feed.feed)}>{feed.feed}</Link>
                        </td>
                        <td>
                            <button className="delete_button" onClick={this.handleDeleteClick} name={feed.id}>X</button>
                        </td>
                    </tr>
                )
            })
        }

        return (
            <div>
                <table id="my_feeds">
                    <tbody>
                        <tr>
                            <th>Link</th>
                            <th>Delete</th>
                        </tr>
                        {rows}
                    </tbody>
                </table>
                <Link to={FEED_CREATION}>
                    <button id="add_feed_button" type="button">Add Feed</button>
                </Link>
                <button className="inversed_button" type="button" onClick={this.handleLogoutClick}>Logout</button>
            </div>
        )
    }
}

const mapStateToProps = ({ auth, feeds }) => {
    return {
        token: auth.token,
        isGetFeedsSuccess: feeds.isGetFeedsSuccess,
        isGetFeedsPending: feeds.isGetFeedsPending,
        isGetFeedsError: feeds.isGetFeedsError,
        feeds: feeds.feeds,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        signOut: (token) => dispatch(signOut(token)),
        getAllFeeds: (token) => dispatch(getAllFeeds(token)),
        deleteFeed: (token, id) => dispatch(deleteFeed(token, id)),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(MyFeeds);
