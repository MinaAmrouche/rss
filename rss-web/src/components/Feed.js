import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import renderHTML from 'react-render-html';

import { getFeedContent } from '../actions';


class Feed extends Component {
    constructor(props) {
        super(props);
        this.state = {
            url: decodeURIComponent(props.match.params.url),
        }
    }

    componentDidMount() {
        this.props.getFeedContent(this.state.url);
    }

    render() {
        let { feedContent, match } = this.props;
        let articles;
        if (feedContent) {
            articles = feedContent.map((article, index) => {
                return (
                    <tr key={index}>
                        <td>
                            <Link to={`${match.url}/${index}`}>
                                <img src={article.thumbnail} alt="Avatar" className="avatar" />
                            </Link>
                        </td>

                        <td>
                            <Link to={`${match.url}/${index}`}>
                                <h2 className="title">{article.title}</h2>
                            </Link>
                            <a className="date">{article.pubDate}</a>
                            <p className="description">
                                {renderHTML(article.content)}
                            </p>
                        </td>
                    </tr>
                )
            })
        }

        return (
            <div>
                <table id="my_feeds">
                    <tbody>
                        <tr>
                            <th>Picture</th>
                            <th>Title</th>
                        </tr>
                        {articles}
                    </tbody>
                </table>
            </div>
        )
    }
}

const mapStateToProps = ({ feeds }) => {
    return {
        feedContent: feeds.feedContent,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        getFeedContent: (url) => dispatch(getFeedContent(url)),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Feed);
