import React, {Component} from 'react';
import { Link, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

import {SIGN_UP, MY_FEEDS} from '../routes';
import { signIn } from '../actions';

class SignIn extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
            signInSuccess: false,
        };
    }

    handleSubmit = (e) => {
        e.preventDefault();
        let { email, password } = this.state;
        this.props.signIn(email, password)
            .then(() => {
                this.setState({signInSuccess: true});
            });
    };

    handleChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        });
    };

    render() {
        let { email, password, signInSuccess } = this.state;

        if (signInSuccess) {
            return (
                <Redirect to={{ pathname: MY_FEEDS }} />
            )
        }

        return (
            <div className="container">
                <form action="my_feeds.html" onSubmit={this.handleSubmit}>
                    <input type="email" placeholder="E-mail" name="email" required onChange={this.handleChange} value={email} />
                    <input type="password" placeholder="Password" name="password" required onChange={this.handleChange} value={password} />
                    <button type="submit">Signin</button>
                </form>
                <Link to={SIGN_UP}>
                    <button type="button" className="inversed_button">Register</button>
                </Link>
            </div>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        signIn: (email, password) => dispatch(signIn(email, password))
    };
};

export default connect(null, mapDispatchToProps)(SignIn);
