export const SIGN_IN = '/signin';
export const SIGN_UP = '/signup';
export const MY_FEEDS = '/';
export const FEED = '/feed';
export const FEED_CREATION = '/add';
export const ARTICLE = '/feed';
