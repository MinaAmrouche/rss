import React, { Component } from 'react';
import { Route } from 'react-router-dom';

import SignIn from './components/SignIn';
import SignUp from './components/SignUp';
import MyFeeds from './components/MyFeeds';
import Feed from './components/Feed';
import FeedCreation from './components/FeedCreation';
import Article from './components/Article';
import * as route from './routes';
import rss_feed from './img/rss_feed.png';

class App extends Component {
  render() {
    return (
      <div>
        <div className="imgcontainer">
          <img src={rss_feed} alt="Avatar" className="avatar"/>
        </div>
        <Route path={route.SIGN_IN} component={SignIn}/>
        <Route path={route.SIGN_UP} component={SignUp}/>
        <Route exact path={route.MY_FEEDS} component={MyFeeds}/>
        <Route exact path={route.FEED + '/:url'} component={Feed}/>
        <Route path={route.FEED_CREATION} component={FeedCreation}/>
        <Route path={route.ARTICLE + '/:url/:id'} component={Article}/>
      </div>
    );
  }
}

export default App;
