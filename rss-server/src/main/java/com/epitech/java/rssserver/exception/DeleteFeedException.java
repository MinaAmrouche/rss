package com.epitech.java.rssserver.exception;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code= HttpStatus.BAD_REQUEST)
public class DeleteFeedException extends RuntimeException {

    public DeleteFeedException() {
        super("You cannot delete this feed.");
    }
}
