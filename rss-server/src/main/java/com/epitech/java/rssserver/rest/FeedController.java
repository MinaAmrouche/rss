package com.epitech.java.rssserver.rest;

import com.epitech.java.rssserver.domain.Feed;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import com.epitech.java.rssserver.service.FeedService;
import com.epitech.java.rssserver.service.UserService;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/feed")
public class FeedController {
    @Autowired
    FeedService feedService;
    
    @Autowired
    UserService userService;

    @PostMapping("/add")
    @ResponseStatus(HttpStatus.CREATED)
    public void addFeed(@RequestHeader(value="token") String token, @RequestParam("feed") String feed) {
        feedService.addFeed(userService.getCurrentUser(token), feed);
    }

    @DeleteMapping("/delete")
    @ResponseStatus(HttpStatus.OK)
    public void removeFeed(@RequestHeader(value="token") String token, @RequestParam("id") int id) {
        feedService.removeFeed(userService.getCurrentUser(token), id);
    }

    @GetMapping("/getAll")
    @ResponseStatus(HttpStatus.OK)
    public List<Feed> getAllFeeds(@RequestHeader(value="token") String token){
    	System.out.println("token:" + token);
    	return feedService.getAllFeeds(userService.getCurrentUser(token));
    	}
}
