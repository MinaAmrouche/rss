package com.epitech.java.rssserver.dao;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.epitech.java.rssserver.domain.Feed;

public interface IFeedDAO extends CrudRepository<Feed, Integer>{
    Feed findById(int id);
}
