package com.epitech.java.rssserver.service;

import javassist.NotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.epitech.java.rssserver.dao.IUserDAO;
import com.epitech.java.rssserver.domain.User;
import com.epitech.java.rssserver.exception.AlreadyExistException;
import com.epitech.java.rssserver.exception.EmptyFieldException;
import com.epitech.java.rssserver.exception.IdNotFoundException;
import com.epitech.java.rssserver.exception.UnauthorizedException;
import com.epitech.java.rssserver.rest.dto.UserDTO;

import org.apache.commons.lang3.RandomStringUtils;

import java.util.Optional;

@Service
@Transactional
public class UserService {

    @Autowired
    private IUserDAO userDAO;

    public UserService() {

    }

    public void registerAccount(UserDTO userDTO) {
        if (checkEmpty(userDTO)) {
            throw new EmptyFieldException();
        }

        userDAO.findByEmail(userDTO.getEmail()).ifPresent(u -> {
            throw new AlreadyExistException();
        });
        userDTO.setPassword(userDTO.getPassword());
        User user = new User(userDTO);
        userDAO.save(user);
    }

    public User getCurrentUser(String token){
    	System.out.println("token:" + token);
    	User user = userDAO.findByToken(token).orElseThrow(() -> new UnauthorizedException());
    	return user;
    }

    private boolean checkEmpty (UserDTO user) {
        if (user == null ||
                user.getUserName() == null || user.getEmail() == null || user.getPassword() == null ||
                user.getUserName().isEmpty() || user.getEmail().isEmpty() || user.getPassword().isEmpty())
            return true;
        return false;
    }

    public Optional<User> getUserByEmail(String email){
        return userDAO.findByEmail(email);
    }

	public User login(String email, String password) {
		if (email == null || password == null || email.equals("") || password.equals(""))
			throw new EmptyFieldException();
		User user = userDAO.findByEmail(email).get();
		if (user == null)
			throw new IdNotFoundException();
		if (!user.getPassword().equals(password))
			throw new UnauthorizedException();
		user.setToken(RandomStringUtils.randomNumeric(20));
		userDAO.save(user);
		return user;
	}

	public void logout(String token) {
		if (token == null || token.equals(""))
			throw new EmptyFieldException();
		User user = getCurrentUser(token);
		user.setToken(null);
		userDAO.save(user);
	}
}
