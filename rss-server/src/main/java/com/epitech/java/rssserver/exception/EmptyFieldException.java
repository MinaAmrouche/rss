package com.epitech.java.rssserver.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code=HttpStatus.BAD_REQUEST)
public class EmptyFieldException extends RuntimeException {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public EmptyFieldException() {
        super("Please fill all field.");
    }
}
