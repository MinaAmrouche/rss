package com.epitech.java.rssserver.service;

import com.epitech.java.rssserver.dao.IFeedDAO;
import com.epitech.java.rssserver.dao.IUserDAO;
import com.epitech.java.rssserver.domain.Feed;
import com.epitech.java.rssserver.domain.User;
import com.epitech.java.rssserver.exception.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
@Transactional
public class FeedService {
    @Autowired
    private IFeedDAO feedDAO;

    @Autowired
    private IUserDAO userDAO;

    @Autowired
    private UserService userService;


    public void addFeed(User user, String feedURL) {
        if (feedURL == null || feedURL.isEmpty()) {
            throw new EmptyFieldException();
        }
        Feed feed = new Feed(feedURL, user);
        if (user.getFeeds().contains(feed))
            throw new AlreadyExistException();
        feedDAO.save(feed);
        user.addFeed(feed);
        userDAO.save(user);
    }

    public void removeFeed(User user, int id) {
        Feed feed = feedDAO.findById(id);
        if (feed == null) {
            throw new IdNotFoundException();
        }
        if (user.getId() != feed.getOwner().getId()){
            throw new DeleteFeedException();
        }
        feedDAO.delete(feed);
        user.removeFeed(feed);
        userDAO.save(user);
    }

    public List<Feed> getAllFeeds(User user){
        if (user.getFeeds().isEmpty())
            throw new NoContentException();
        return user.getFeeds();
    }

}
