package com.epitech.java.rssserver.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.epitech.java.rssserver.domain.User;
import com.epitech.java.rssserver.rest.dto.UserDTO;
import com.epitech.java.rssserver.service.UserService;

@CrossOrigin
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    UserService userServices;

    @PostMapping("/signIn")
    @ResponseStatus(HttpStatus.OK)
    public User login(@RequestParam("email") String email, @RequestParam("password") String password) {
        return userServices.login(email, password);
    }
    
    @PostMapping("/signUp")
    @ResponseStatus(HttpStatus.CREATED)
    public void register(@RequestBody UserDTO userDTO) {
        userServices.registerAccount(userDTO);
    }
    
    @GetMapping("/logout")
    @ResponseStatus(HttpStatus.OK)
    public void logout(@RequestHeader(value="token") String token) {
        userServices.logout(token);
    }
}
