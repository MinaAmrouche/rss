package com.epitech.java.rssserver.dao;

import java.util.Optional;

import com.epitech.java.rssserver.domain.User;
import org.springframework.data.repository.CrudRepository;


public interface IUserDAO extends CrudRepository<User, Integer>{

    Optional<User> findByEmail(String email);
    
    Optional<User> findByToken(String token);
}