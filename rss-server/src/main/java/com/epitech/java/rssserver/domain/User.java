package com.epitech.java.rssserver.domain;


import com.epitech.java.rssserver.rest.dto.UserDTO;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Entity
public class User {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private int id;

    @Column
    @NotNull
    private String userName;

    @Column
    @NotNull
    private String email;

    @Column
    @NotNull
    @JsonIgnore
    private String password;
    
    @Column
    private String token;

    @OneToMany(mappedBy = "owner", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JsonIgnore
    private List<Feed> feeds = new ArrayList<Feed>();

    public User(){}

    public User(String userName, String email, String password, String token){
        this.userName = userName;
        this.email = email;
        this.password = password;
        this.token = token;
    }

    public User(UserDTO userDTO) {
        this.userName = userDTO.getUserName();
        this.email = userDTO.getEmail();
        this.password = userDTO.getPassword();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<Feed> getFeeds() {
        return feeds;
    }

    public void setFeeds(List<Feed> feeds) {
        this.feeds = feeds;
    }

    public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public void   addFeed(Feed feed){
        this.feeds.add(feed);
    }

    public void removeFeed(Feed feed) {
        Feed feedToRemove = null;
        for (Feed feedElem : this.feeds)
            if (feed.getId() == feedElem.getId()) {
                feedToRemove = feedElem;
                break;
            }
        if (feedToRemove != null)
            this.feeds.remove(feedToRemove);
    }
}
