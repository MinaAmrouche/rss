let baseURL = "http://212.227.53.116:8080/";

angular.module('app.services', [])

  .factory('UsersServices', function ($http) {
    return {
      register: function (data) {
        return $http.post(baseURL + "user/signUp", data);
      },
      login: function (data) {
        return $http.post(baseURL + "user/signIn?email=" + data.email + "&password=" + data.password, null);
      },
      logout: function () {
        return $http.post(baseURL + "user/signOut", null);
      },
    };
  })

  .factory('FeedsServices', function ($http) {
    return {
      getFeeds: function(token) {
        let headers = { 'token' : token };
        return $http.get(baseURL + "feed/getAll", {headers});
      },
      addFeed: function(token, url) {
        let headers = { 'token' : token };
        return $http.post(baseURL + "feed/add?feed=" + url, null, { headers })
      },
      deleteFeed: function(token, id) {
        let headers = { 'token' : token };   
        return $http.delete(baseURL + "feed/delete?id=" + id, { headers })
      }
    };
  })

  .factory('ArticlesFactory', ['$http', function ($http) {
    var rss_converter = "https://rss2json.com/api.json?rss_url=";
    return {
      getArticlesTitles: function (url) {
        return $http.get(rss_converter + encodeURIComponent(url));
      }
    };
  }])
