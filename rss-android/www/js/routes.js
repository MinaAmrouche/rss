angular.module('app.routes', [])

  .config(function ($stateProvider, $urlRouterProvider) {

    // Ionic uses AngularUI Router which uses the concept of states
    // Learn more here: https://github.com/angular-ui/ui-router
    // Set up the various states which the app can be in.
    // Each state's controller can be found in controllers.js
    $stateProvider


      .state('signup', {
        url: '/signup',
        templateUrl: 'templates/signup.html',
        controller: 'signupCtrl'
      })

      .state('login', {
        url: '/login',
        templateUrl: 'templates/login.html',
        controller: 'loginCtrl'
      })

      .state('myFeeds', {
        url: '/feeds',
        templateUrl: 'templates/myFeeds.html',
        controller: 'myFeedsCtrl',
        params: { username: null , token: null}
      })

      .state('addFeed', {
        url: '/add',
        templateUrl: 'templates/addFeed.html',
        controller: 'addFeedCtrl',
        params: { username: null , token: null}
      })

      .state('feedArticles', {
        url: '/feed',
        templateUrl: 'templates/feedArticles.html',
        controller: 'feedArticlesCtrl',
        params: { username: null , token: null, feed: null, url: null }
      })

      .state('articleDetails', {
        url: '/details',
        templateUrl: 'templates/articleDetails.html',
        controller: 'articleDetailsCtrl',
        params: { username: null , token: null, article: null}
      })

    $urlRouterProvider.otherwise('/login')


  });