angular.module('app.controllers', [])
  
.controller('signupCtrl', function ($scope, $stateParams, $state, UsersServices, $ionicLoading, $ionicPopup) {
    $scope.user = { userName: null, email: null, password: null };
    $scope.signup = function() {
        $ionicLoading.show({
            template: 'Signin Up...'
        });
        UsersServices.register($scope.user)
        .then(function(res) {
            console.log("RES SIGNUP : ", res);
            $state.go('login');
        })
        .catch(function(err) {
            console.log("ERROR SIGNUP : ", err);
            $ionicPopup.show({
                template: 'Error',
                title: 'Signup',
                buttons: [{text: 'Close'}]
            });
        })
        .finally(function() {
            $ionicLoading.hide();
        });
    };


})
   
.controller('loginCtrl', function ($scope, $stateParams, $state, UsersServices, $ionicLoading, $ionicPopup) {
    $scope.user = { email: null, password: null };
    $scope.signin = function() {
        $ionicLoading.show({
            template: 'Logging In...'
        });
        UsersServices.login($scope.user)
        .then(function(res) {
            console.log("RES LOGIN : ", res);
            $state.go('myFeeds', { username: res.data.userName, token: res.data.token });
        })
        .catch(function(err) {
            console.log("ERROR LOGIN : ", err);
            $ionicPopup.show({
                template: "Couldn't login",
                title: 'Login',
                buttons: [{text: 'Close'}]
            });
        })
        .finally(function() {
            $ionicLoading.hide();
        });
    };
})
   
.controller('myFeedsCtrl', function ($scope, $stateParams, FeedsServices, $state) {
    $scope.getAllFeeds = function () {
        FeedsServices.getFeeds($stateParams.token)
            .then(function (res) {
                console.log(res);
                $scope.feeds = res.data;
            })
            .catch(function (err) {
                console.log("Connection error : ");
            })
            .finally(function () {
            });
    };
    $scope.getAllFeeds();

    $scope.deleteFeed = function(id) {
        FeedsServices.deleteFeed($stateParams.token, id)
            .then(function (res) {
                $scope.getAllFeeds();
            })
            .catch(function (err) {
                console.log("Connection error : ");
            })
            .finally(function () {
            });        
    }

    $scope.goToAdd = function () {
        $state.go('addFeed', { username: $stateParams.username, token: $stateParams.token });        
    }

    $scope.goToFeed = function (url) {
        $state.go('feedArticles', { username: $stateParams.username, token: $stateParams.token, url: url });
    };
})

.controller('addFeedCtrl', function ($scope, $stateParams, FeedsServices, $state) {
    $scope.feed = { url: null };
    $scope.addFeed = function () {
        FeedsServices.addFeed($stateParams.token, $scope.feed.url)
            .then(function (res) {
                $state.go('myFeeds', { username: $stateParams.username, token: $stateParams.token });
            })
            .catch(function (err) {
                console.log("ERROR ");
            })
            .finally(function () {
            });
    };
})
   
.controller('feedArticlesCtrl', function ($scope, $stateParams, ArticlesFactory, $state) {
    $scope.currentFeed = $stateParams.feed;
    $scope.loadTitles = function () {
        ArticlesFactory.getArticlesTitles($stateParams.url)
            .then(function (res) {
                console.log(res.data.items);
                if (res.data.items) {
                    $scope.titles = res.data.items;
                } else {
                    console.log("Feed error : " + res.data);
                }
            })
            .catch(function (err) {
                console.log("Connection error : ");
            })
            .finally(function () {
            });
    };
    $scope.loadTitles();


    $scope.goToArticle = function (article) {
        $state.go('articleDetails', { username: $stateParams.username, token: $stateParams.token, article: article });
    };


})
   
.controller('articleDetailsCtrl', function ($scope, $stateParams) {
    $scope.article = $stateParams.article;
})
 