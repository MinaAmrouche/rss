#!/usr/bin/env bash

until cd /var/www && mvn package
do
    echo "Retrying mvn package"
done
java -Djava.security.egd=file:/dev/./urandom -jar target/rss-server-0.0.1.jar