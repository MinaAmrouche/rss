.DEFAULT_GOAL := help
SHELL=/bin/bash

export USER_UID=$(shell id -u)
DOCKER_DIR=infra/dev
DOCKER_PREFIX=rss
DOCKER_COMPOSE_BIN=cd ${DOCKER_DIR} && docker-compose -p ${DOCKER_PREFIX}
BASH_CONTAINER=server


### PROJECT
# ¯¯¯¯¯¯¯¯¯

setup: pull build up ## Setup the development environment (please refer to the README.md)

ionic-server: 
	cd rss-android && ionic serve

react-server:
	cd rss-web && yarn start

### DOCKER
# ¯¯¯¯¯¯¯¯

pull: ## Pull the external images
	${DOCKER_COMPOSE_BIN} pull

build: ## Build the containers and pull FROM statements
	${DOCKER_COMPOSE_BIN} build --pull

up: ## Up the containers
	${DOCKER_COMPOSE_BIN} up -d

down: ## Down the containers (keep volumes)
	${DOCKER_COMPOSE_BIN} down

destroy: ## Destroy the containers, volumes, networks…
	${DOCKER_COMPOSE_BIN} down -v --remove-orphan

start: ## Start the containers
	${DOCKER_COMPOSE_BIN} start

stop: ## Stop the containers
	${DOCKER_COMPOSE_BIN} stop

restart: ARGS =
restart: ## Restart the containers. Use ARG="container-name" if you want to be specific.
	${DOCKER_COMPOSE_BIN} restart ${ARGS=""}

bash: ## Run bash shell on the "bash" container (user www-data)
	${DOCKER_COMPOSE_BIN} exec --user www-data ${BASH_CONTAINER} bash

rbash: ## Run bash shell on the "bash" container (user root)
	${DOCKER_COMPOSE_BIN} exec --user root ${BASH_CONTAINER} bash

.PHONY: logs
logs: ## Show containers logs
	${DOCKER_COMPOSE_BIN} logs -f

dc: ARGS = ps
dc: ## Run docker-compose command. Use ARGS="" to pass parameters to docker-compose.
	${DOCKER_COMPOSE_BIN} ${ARGS}

### OTHERS
# ¯¯¯¯¯¯¯¯

.PHONY: help
help: ## Dislay this help
	@IFS=$$'\n'; for line in `grep -E '^[a-zA-Z_#-]+:?.*?## .*$$' $(MAKEFILE_LIST)`; do if [ "$${line:0:2}" = "##" ]; then \
	echo $$line | awk 'BEGIN {FS = "## "}; {printf "\n\033[33m%s\033[0m\n", $$2}'; else \
	echo $$line | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-25s\033[0m %s\n", $$1, $$2}'; fi; \
	done; unset IFS;
